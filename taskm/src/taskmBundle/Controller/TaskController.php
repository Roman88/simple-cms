<?php

namespace taskmBundle\Controller;

use taskmBundle\Repository\CategoryRepository;
use taskmBundle\Repository\TaskRepository;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use taskmBundle\Entity\Task;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


class TaskController extends Controller
{
    public function indexAction()
    {
        $cat = $this->getDoctrine()->getRepository('taskmBundle:Category')->findAll();
        $tasks = $this->getDoctrine()->getRepository('taskmBundle:Task')->findAll();
        // echo date_format($news[0]->getPtime(), 'Y-m-d');
        return $this->render('taskmBundle:Task:index.html.twig', array(
            'category' => $cat,
            'tasks' => $tasks
        ));
    }

    public function oneAction($url)
    {
        $entity =  $this->getDoctrine()->getRepository('taskmBundle:Task')->findOneByName($url);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }
        return $this->render('taskmBundle:Task:one.html.twig', array(
            'entity'      => $entity,
        ));
    }
}

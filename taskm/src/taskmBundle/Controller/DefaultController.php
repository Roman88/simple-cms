<?php

namespace taskmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('taskmBundle:Default:index.html.twig', array('name' => $name));
    }
}

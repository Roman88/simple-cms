<?php
namespace taskmBundle\Controller;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\ORM\Mapping\Id;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;


class FController extends Controller
{

    static $upath= '/upload/' ;

    public function uploadAction(Request $request)
    {
        $dir =  $this->getParameter('web_dir').self::$upath;
        /** @var UploadedFile $file */
        $file =  $request->files->get('upload');
        $file->move($dir,$file->getClientOriginalName());

        $message = 'Файл успешно загружен!';

        echo "<script> alert ('".$message."');</script>";
        return new Response(
            '<script type=\'text/javasphpcript\'>window.parent.CKEDITOR.tools.callFunction('
            .', \''
            .'\', \''
            .$message.'\');</script>'
        );
    }

    public function browseAction(Request $request)
    {
        $dir = $this->getParameter('web_dir').self::$upath;
        $fileItems = scandir ($dir);
        foreach ($fileItems as $key => $fileItem) {
            if (!preg_match('/\.(jpg|jpeg|png|gif)$/i', $fileItem)) {
                continue;
            }
            $files[] = [ 'src' => 'http://'.$_SERVER['HTTP_HOST'].self::$upath.$fileItem , 'id' => $key];
        }

        return $this->render('taskmBundle:Task:ckeditor_browse.html.twig', [ 'images' => $files ]);

    }

    public function removeAction($id)
    {
        $this->get('file_storage.helper')->remove($id);
        return $this->redirectToReferer();
    }


}
<?php

namespace taskmBundle\Services\Extension;

use \Twig_Filter_Method;
use taskmBundle\Entity;
use PDO;


use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


class TwigExtension extends \Twig_Extension
{

    protected $doctrine;



    /**
     * Return the functions registered as twig extensions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'file_exists' => new \Twig_Function_Function('file_exists'),
            'classif' => new \Twig_Function_Method($this, 'classif'),
            'show_authors' => new \Twig_Function_Method($this, 'show_authors'),
            'is_in_array' => new \Twig_Function_Method($this, 'is_in_array'),
            'get_grif' => new \Twig_Function_Method($this, 'get_grif'),
            'cut_title' => new \Twig_Function_Method($this, 'cut_title'),
            'get_tcode' => new \Twig_Function_Method($this, 'get_tcode'),
            'get_tcard' => new \Twig_Function_Method($this, 'get_tcard'),
            'get_price' => new \Twig_Function_Method($this, 'get_price'),
            'get_isbn' => new \Twig_Function_Method($this, 'get_isbn'),
            'get_publishers' => new \Twig_Function_Method($this, 'get_publishers'),
            'get_deliver' => new \Twig_Function_Method($this, 'get_deliver'),
            'formatPrice' => new \Twig_Function_Method($this, 'formatPrice'),
            'pubImgSrc' => new \Twig_Function_Method($this, 'get_publication_image'),
            'menu_cat' => new \Twig_Function_Method($this, 'menu_cat'),
//            'breadcrumbs' => new \Twig_Function_Method($this,'breadcrumbs'),
        );
    }

    public function menu_cat($item = null)
    {
        $str= '';
        return $str;
    }

    public function getFilters()
    {
        return array(
            'entity' => new \Twig_Filter_Method($this, 'publication_entity'),
            'publication_date' => new \Twig_Filter_Method($this, 'get_publication_date'),
            'publication_type' => new \Twig_Filter_Method($this, 'get_publication_type'),
            'addslashes' => new \Twig_Filter_Method($this, 'addslashes')
        );
    }


    public function addslashes($string)
    {
        return addslashes($string);
    }


    public function is_in_array($array, $item)
    {
        $array = is_array($array) ? $array : [$array];
        return in_array($item, $array);
    }



    /**
     * @param $item_id
     * @return int
     */
    public function show_authors($item_id)
    {
        // 1 - Автор
        // 2 - Соавтор

        $manager = $this->doctrine->getManager();
        $QBuilder = $manager->
        getRepository('DataBundle:Author')
            ->createQueryBuilder('a')
            ->select('a.last_name, a.first_name, a.second_name')
            ->addSelect('at.type_no, at.name, at.onix_code')
            ->join('a.publications', 'pa')
            ->join('pa.author_type', 'at')
            ->Where('pa.publication = :id')
            ->setParameter(':id', $item_id);

        $res = $QBuilder->getQuery()->getArrayResult();

        // Сортируем массив по типу авторства
        usort($res, function ($b, $c){
            if ($b['type_no'] == $c['type_no']) {
                return 0;
            }
            return ($b['type_no'] < $c['type_no']) ? -1 : 1;
        });

        $first_auth = $second_auth = $custom_auth = $transl_auth = $compil_auth = '';
        $aut_cnt = $custom_cnt = $transl_cnt = $compil_cnt =  0;
        foreach ($res as $author) {

            if (in_array($author['onix_code'], ['A01', 'A03', 'A05', 'A06', 'A07', 'A10', 'A11', 'A12',
                'A13', 'A15', 'A18', 'A19', 'A20', 'A21', 'A23', 'A24', 'A32', 'A33', 'A36', 'A37'])) {

                if ($author['type_no'] == 1) {
                    $first_auth .= $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.mb_substr($author['second_name'], 0, 1).'., ';
                    $aut_cnt++;
                } elseif ($author['type_no'] == 2) {
                    $second_auth .= $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.mb_substr($author['second_name'], 0, 1).'., ';
                    $aut_cnt++;
                }
            } elseif(in_array($author['onix_code'], ['B01', 'B21'])) {
                $title = $author['name'];
                $custom_auth .= $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.mb_substr($author['second_name'], 0, 1).'., ';
                $custom_cnt++;
            } elseif ($author['onix_code'] == 'B06') {
                $title = 'Перевод';
                $transl_auth .= $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.mb_substr($author['second_name'], 0, 1).'., ';
                $transl_cnt++;
            } elseif ($author['onix_code'] == 'C01') {
                $title = 'Составитель';
                $compil_auth .= $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.mb_substr($author['second_name'], 0, 1).'., ';
                $compil_cnt++;
            }
        }
//        $final_result = mb_substr($first_author.' '.$second_author, 0, -1);
        $final_result = '';

        if ($aut_cnt > 0) {
            $final_result .= /*'<tr><td>Автор</td><td>'.*/mb_substr($first_auth.$second_auth, 0, -1).'</td></tr>';
        }
        if ($custom_cnt > 0) {
            $final_result .= '<tr><td>'.$title.'</td><td>'.mb_substr($custom_auth, 0, 1).'</td></tr>';
        }
        if ($transl_cnt > 0) {
            $final_result .= '<tr><td>Перевод</td><td>'.mb_substr($transl_auth, 0, -1).'</td></tr>';
        }
        if ($compil_cnt > 0) {
            $final_result .= '<tr><td>Составитель</td><td>'.mb_substr($transl_auth, 0, -1).'</td></tr>';
        }
//        return $res;
        return $final_result;
    }

    public function classif($item, $classificator)
    {
        return false;
    }

    public function formatPrice($price)
    {
        if (preg_match("/\./", $price)) {
            $price = sprintf('%.2f', $price);
        }
        return $price;
    }

    public function get_publication_type($item)
    {
        switch ($item) {
            case $item instanceof Entity\Monography:
                return 'Монография';
            case $item instanceof Entity\Article:
                return 'Статья';
            case $item instanceof Entity\Textbook:
                return 'Учебное пособие';
            case $item instanceof Entity\MagazineMatter:
                return 'Журнальный материал';
            case $item instanceof Entity\Book:
                return 'Книга';
            default:
                return false;
        }
    }

    public function get_publication_date($date)
    {
        $month =  ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

        return $month[ $date->format('m') - 1 ].$date->format(' Y');
    }

    public function cut_title($title, $len)
    {
        if (strlen($title) > (int)$len) {
            mb_internal_encoding("UTF-8");
            return mb_substr($title, 0, (int)$len).'...';
        } else {
            return $title;
        }
    }

    public function get_publishers($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select o.title_rus
from publication_publishers pp
join organization o ON o.id = pp.publisher_id and o.discr = "publisher"
where  pp.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $publishers = $stmt->fetchAll(PDO::FETCH_COLUMN);

        $count = 0;
        $last = count($publishers) - 1;
        $publishers_res = '';

        foreach ($publishers as $i) {
            $publishers_res .= $i;
            if ($last != $count) $publishers_res .= ', ';
            $count++;
        }

        return $publishers_res;
    }

    public function get_tcode($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select tr.trade_code
from publication_trade tr
where tr.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $tcode = $stmt->fetch(PDO::FETCH_COLUMN);

        return $tcode;
    }

    public function get_tcard($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select tr.trade_card
from publication_trade tr
where tr.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $tcard = $stmt->fetch(PDO::FETCH_COLUMN);

        return $tcard;
    }

    public function get_price($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select pr.value
from publication_prices pr
where pr.type_id = 1
and pr.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $price = $stmt->fetch(PDO::FETCH_COLUMN);

        $price = $this->formatPrice($price);

        return $price;
    }

    public function get_isbn($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select pi.isbn
from publication_isbns pi
where  pi.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $isbn = $stmt->fetchAll(PDO::FETCH_COLUMN);
        $count = 0;
        $last = count($isbn) - 1;
        $isbn_res = '';

        foreach ($isbn as $i) {
            $isbn_res .= $i;
            if ($last != $count) $isbn_res .= ', ';
            $count++;
        }
        return $isbn_res;
    }

    public function get_grif($grif)
    {
        if ($grif === '0')
            return 'нет';
        else
            return 'есть';
    }

    public function get_deliver($item_id)
    {
        $pdo = $this->getPDO();

        $stmt = $pdo->prepare('
select ptr.publication_id
from publication_trade ptr
join publication_availability av ON ptr.publication_id = av.publication_id
where ptr.artnoadd2 = 98
and av.quantity = 0
and ptr.publication_id = :item_id
');

        $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);
        $stmt->execute();
        $need = $stmt->fetch(PDO::FETCH_COLUMN);
        if ($need != false) {
            return 'Cрок поставки 1-2 недели';
        } else {
            return '';
        }
    }

    public function get_publication_image($id)
    {
        $link = 'catalogImg/NoImg.png';

        $id = (int)$id;
        $dir1000 = sprintf('%04d', intval($id / 1000));
//        $target_id = sprintf('%07d', $id);
//        $img = $id - $dir1000*1000;
        $file = 'catalogImgUPP/'.$dir1000.'/'.$id.'.jpg';

        if (file_exists($file)) {
            $link = $file;
        }
        return '/'.$link;
    }

    public function publication_entity($item)
    {
        switch ($item) {
            case $item instanceof Entity\Book:
                return 'Book';
            case $item instanceof Entity\Monography:
                return 'Monography';
            case $item instanceof Entity\Article:
                return 'Article';
            case $item instanceof Entity\Textbook:
                return 'Textbook';
            case $item instanceof Entity\MagazineMatter:
                return 'Magazinematter';
            default:
                return false;
        }
    }


    public function getName()
    {
        return 'twig_extension';
    }

    public function getPDO()
    {
        $host = $this->container->getParameter('database_host');
        $db_name = $this->container->getParameter('database_name_catalog');
        $user = $this->container->getParameter('database_user');
        $pass = $this->container->getParameter('database_password');

        try {

            $pdo = new PDO("mysql:host={$host};dbname={$db_name}", $user, $pass);
            $pdo->exec('set names utf8');


        } catch (PDOException $e) {

            $pdo = "Error!: " . $e->getMessage() . "<br/>";
        }
        return $pdo;
    }
}
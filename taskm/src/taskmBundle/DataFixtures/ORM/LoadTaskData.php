<?php



namespace taskmBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use taskmBundle\Entity\Task;

class LoadTaskData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $TaskFullTime = new Task();
        $TaskFullTime->setCategory($em->merge($this->getReference('category-programming')));
        $TaskFullTime->setName("Задача 1");

        $TaskPartTime = new Task();
        $TaskPartTime->setCategory($em->merge($this->getReference('category-design')));
        $TaskPartTime->setName('Задача2');

        $em->persist($TaskFullTime);
        $em->persist($TaskPartTime);
        $em->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
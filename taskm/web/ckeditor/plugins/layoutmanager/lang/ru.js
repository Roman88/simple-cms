CKEDITOR.plugins.setLang('layoutmanager', 'ru', {
    title: 'Оформление',
    addLayoutDialogTitle: 'Избери шаблон',
    manageLayoutDialogTitle: 'Замени с',
    removeLayoutMenuLabel: 'Премахни шаблон',
    manageLayoutMenuLabel: 'Замени шаблон'
});

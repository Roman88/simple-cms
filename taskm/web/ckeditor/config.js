/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.skin = 'bootstrapck';
    config.extraPlugins = 'mathjax,texzilla,symbol,chart,leaflet,widget,sharedspace,dialogui,lineutils,codesnippet,uploadimage,uploadwidget,filetools,notification,notificationaggregator,toolbar,button,filebrowser,popup,layoutmanager,richcombo,listblock,panel,floatpanel,letterspacing,lineheight';

    config.mathJaxLib = 'http://cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML';

    config.imageUploadUrl = '/uploader/u';
    config.filebrowserBrowseUrl = '/browser/b';
    //config.filebrowserImageBrowseUrl = '/browser/b?type=Images';
    config.filebrowserUploadUrl = '/uploader/u';
    //config.filebrowserImageUploadUrl = '';

    config.layoutmanager_loadbootstrap = true;

    config.toolbarGroups = [
        { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'basicstyles' },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'insert' },
        '/',
        { name: 'styles' },
        { name: 'others' },
        { name: 'colors' },
        { name: 'tools' }
    ];

    config.removePlugins = 'smiley,specialchar,forms,language,flash';
};
